# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

功能：
    ● 調整筆刷大小
    ● 調整筆刷顏色
    ● 橡皮擦
    ● 彩虹畫筆
    ● 輸入文字(包含字型、字體、顏色)
    ● 清空畫布
    ● 復原(undo)
    ● 取消復原(redo)
    ● 圓形
    ● 矩形
    ● 三角形
    ● 下載圖檔
    ● 上傳圖檔
    ● 鼠標轉換
功能介紹：
    1.筆刷
        使用三個Listener，分別為mousedown,mousemove,mouseup
        當mousedown時，把flag設為true
        當mousemove時，如果flag為true，則循環draw的function，以lineTo劃出線
        當mouseup時，把flag設為false
    2.橡皮擦
        同筆刷，但把顏色設為白色
    3.筆刷顏色及大小
        在網路上搜尋到color-picker，使用strokeStyle來改顏色
        大小則是用lineWidth來改
    4.彩虹畫筆
        與筆刷相似，不同之處在於顏色要隨滑鼠移動而改變，且每一次mousemove要closePath
        接下來要beginPath和移動畫筆(moveTo)
        否則會造成整條線一齊變色
    5.清空畫布
        把整片畫布變成白色即可
    6.復原、取消復原
        在每一次的操作中，使用canvas.toDataURL()把圖檔存進一個array
        當復原或是取消復原時，利用drawImage把上一步或是下一步圖檔畫上去
##      prepare_push() undo() redo()
    7.矩形
        與復原類似的想法，當mousedown時，把圖檔儲存
        mousemove時，先以之前儲存的圖檔覆蓋，然後畫出矩形
        即可如小畫家般完成拖曳畫出矩形的功能
##      rect_load() draw_rect()
    8.三角形
        同矩形，但是在mousemove階段，畫出圖形後
        要closePath然後再beginPath
        才不會造成線全部連在一起
##      rect_load() draw_triangle()
    9.圓形
        同三角形，差別只在於使用arc()
##      rect_load() draw_circle()
###     rect_load()的功能是以儲存圖檔覆蓋畫布，矩形、三角形、圓形三者皆會用到，且內容相同
    10.下載圖檔
        原本是用<a>裡面的download，但無法以button方式呈現
        後改成先使用button，當onclick時，觸發downloadpic()
        使用createElement，創造一個<a>，並加上download等屬性
        最後使用click()觸發<a>，下載圖檔
        不過這裡遇到了一個無法理解的問題 (問題1)
    11.上傳圖檔
        使用input type="file"的方式，配合FileReader完成。
        但是無法修改成自行設計的icon
        所以把這個file input放在隨便一個地方，並在css display設為none
        然後在原位置加入一個button，設定onclick = imagineLoader.click()
##      handleImage(e)                         `↑此為file input之ID`
    12.輸入文字
        當mousedown時，會在點擊處以createElement創造一個<input>
        使用setAttribute把型態設為text，後以appendChild加進div或是body中
        以css position = absolute，並設定其top,left值，以固定對話框
        之後增加一個文字框的keydown listener，若按下Enter鍵之後，
        則使用fillText把文字畫到畫布上，且移除listener
        最後利用removeChild移除文字框(input)
##      texting() isText(e) createElement('INPUT')   
##      setAttribute('type','text') appendChild(text) removeChild(text_) 
    13.字體字型顏色改變
        顏色使用筆刷的顏色
        字體字型使用font去改變
    14.鼠標轉換
        在畫布以外的地方為正常游標
        而在畫布之中為十字游標
        實現方法在css 畫布之中
        當hover時 把cursor設為crosshair
問題：
    問題1.downloadpic()
        在把<a>改成使用button下載時，先手刻了一個function
        然而卻跑出error訊息
        而後我把從網路上抓下來，幾乎一模一樣的function複製貼上
        卻可以正常執行

        接下來我把手刻的那個function改成跟網路上的一模一樣
        連變數、排版都設成一模一樣
        手刻的那份卻依然跑出error訊息
        而網路上的那份仍然可以正常執行

        已詢問過助教、也請同學幫忙檢查過了
        錯誤訊息為下
        not allowed to navigate top frame to data url

        兩個function的內容也在.js檔案之中了
