console.log("hello");
var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var e = event;
var mode = 0;
var push = false;
var R = 255;
var G = 0;
var B = 0;
var now_x = 0;
var now_y = 0;
var cPushArray = new Array();
var cStep = -1;
var rPushArray = new Array();
var ctx_size = document.getElementById('ctx_size');
    ctx_size.addEventListener('change',change_size);
var ctx_font = document.getElementById('theFont');
    ctx_font.addEventListener('change',change_font);
var ctx_font_size = document.getElementById('font_size');
    ctx_font_size.addEventListener('change',change_font);
ctx.fillStyle = 'red';
ctx.lineWidth = 5;
ctx.font = "16pt Arial";
var IsTexting = false;
var text_x;
var text_y;

function change_font(){
  ctx.font = `${ctx_font_size.value}pt ${ctx_font.value}`;
}

function change_size(n){
  var ctx_size_value = parseInt(ctx_size.value);
  switch(n){
    case 1:
      if(ctx_size.value >10){ctx_size.value = ctx_size_value - 10;}
    break;
    case 2:
      if(ctx_size.value <100){ctx_size.value = ctx_size_value + 10;}
    break;
  }
  ctx.lineWidth = ctx_size.value/10;
}

function handleImage(e){
  var reader = new FileReader();
  reader.onload = function(event){
      var pic = new Image();
      pic.onload = function(){
          ctx.drawImage(pic,0,0);
      }
      pic.src = event.target.result;
  }
  
  reader.readAsDataURL(e.target.files[0])/*.then((res)=>{
    console.log(e.target.files);
  });    */
}

function change_mode(str){
  switch(str){
    case 'normal':
      mode = 1;
    break;
    case 'eraser':
      mode = 2;
    break;
    case 'rainbow':
      mode = 3;
    break;
    case 'rect':
      mode = 4;
    break;
    case 'circle':
      mode = 5;
    break;
    case 'triangle':
      mode = 6;
    break;
    case 'text':
      mode = 7;
    break;
    default:
  }
}

function draw_triangle(x,y){
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY; 
  ctx.moveTo((mouse.x+x)/2,y);
  ctx.lineTo(x,mouse.y);
  ctx.lineTo(mouse.x,mouse.y);
  rect_load();
  setTimeout(()=>{
    ctx.closePath();
    ctx.stroke();
    ctx.beginPath();
  },5);
}

function draw_circle(x,y){
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY; 
  var _x = Math.abs(x-mouse.x);
  var _y = Math.abs(y-mouse.y);
  var r = Math.sqrt(Math.pow(_x,2)+Math.pow(_y,2));
  ctx.arc((x+mouse.x)/2,(y+mouse.y)/2,r/2,0,2*Math.PI);
  rect_load();
  setTimeout(()=>{
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
  },5);
}

function clear_canvas(){
  ctx.fillStyle = '#fff';
  ctx.fillRect(0,0,canvas.width,canvas.height);
  prepare_push();
}

function prepare_push(){
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(canvas.toDataURL());
}


function undo(){
  if (cStep > 0) {
    cStep--;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}

function rect_load(){
  var canvasPic = new Image();
  canvasPic.src = rPushArray[0];
  canvasPic.onload= function () { ctx.drawImage(canvasPic, 0, 0); }    
}

function redo(){
  if (cStep < cPushArray.length-1) {
    cStep++;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}
var te = document.getElementById('body');
function texting(){
  var text = document.createElement('INPUT');
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY;
  text.setAttribute('type','text');
  var rec = canvas.getBoundingClientRect();
  te.appendChild(text);
  text.id = 'text_';
  text.style.position = "absolute";
  text.style.top = `${mouse.y+rec.top}px`;
  text.style.left = `${mouse.x+rec.left}px`;
  text.style.border = "solid 1px #000000";
  text.style.zIndex=9;
  text_x = mouse.x;
  text_y = mouse.y;
  text.addEventListener('keypress',isText);
}

function isText(e){
  var text_ = document.getElementById('text_');
  var rec = canvas.getBoundingClientRect();
  if(e.keyCode == 13){
    ctx.fillText(text_.value,text_x,text_y);
    te.removeChild(text_);
    IsTexting = false;
    prepare_push();
  }
}
/*     見鬼了我的天 下面兩個function一模一樣
上面是我手刻的 會報error
下面網路拿下來的 可以正常下載

function downloadpic()
{
    image = canvas.toDataURL();
    var link = document.createElement('a');
    link.downlaod = "my-image.png";
    link.href = image;
    link.click();
}*/

function downloadpic()
{
    image = canvas.toDataURL();
    var link = document.createElement('a');
    link.download = "my-image.png";
    link.href = image;
    link.click();
}

function draw_rainbow(){
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY;
  ctx.lineTo(mouse.x,mouse.y);
  if(R == 255 && 255>G &&G >= 0 && B == 0){
    G = G + 3;
  }
  else if(0<R && R <= 255 && G == 255 && B == 0){
    R = R - 3;
  }
  else if(R == 0 && G == 255 && 255>B && B >= 0){
    B = B + 3;
  }
  else if(R == 0 && 0<G && G<= 255 && B == 255){
    G = G - 3;
  }
  else if(255>R && R >= 0 && G == 0 && B == 255){
    R = R + 3;
  }
  else if(R == 255 && G == 0 && 0<B && B <= 255){
    B = B - 3;
  }
  ctx.strokeStyle = 'rgb(' + R + ',' + G + ',' + B +')';
  ctx.closePath();
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(mouse.x,mouse.y);
}

function draw_rect(x,y){
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY;
  rect_load();
  setTimeout(()=>{ctx.strokeRect(x,y,mouse.x-x,mouse.y-y)},5);
}

function normal(){
  var mouse={
    x:0,
    y:0
  }
  mouse.x = event.offsetX;
  mouse.y = event.offsetY;
  ctx.lineTo(mouse.x,mouse.y);
  ctx.stroke();
}
function eraser(){
  var mouse={
    x:0,
    y:0
  }
  ctx.strokeStyle='#fff';
  mouse.x = event.offsetX;
  mouse.y = event.offsetY;
  ctx.lineTo(mouse.x,mouse.y);
  ctx.stroke();
}
window.onload = function(){
  ctx.fillStyle = '#fff';
  ctx.fillRect(0,0,canvas.width,canvas.height);
  prepare_push();
}
window.addEventListener('load',function (){
  canvas.addEventListener('mousedown',function(){
    push = true;
    var mouse={
      x:0,
      y:0
    }
    mouse.x = event.offsetX;
    mouse.y = event.offsetY;
    ctx.beginPath();
    ctx.moveTo(mouse.x,mouse.y);
    now_x = mouse.x;
    now_y = mouse.y;
    if(mode == 4 ||mode == 5 || mode == 6){
      rPushArray[0]= canvas.toDataURL();
    }
    if(mode == 7){
      push = false;
      if(!IsTexting){
        IsTexting = true;
        texting();
        if(!IsTexting){
          var text = document.getElementById('text_');
          text.removeEventListener('keypress',isText);
        } 
      }    
    }
  })
  canvas.addEventListener('mousemove',function(){
    if(push){
      switch(mode){
        case 1:
          normal();
        break;
        case 2:
          eraser();
        break;
        case 3:
          draw_rainbow();
        break;
        case 4:
          draw_rect(now_x,now_y);
        break;
        case 5:
          draw_circle(now_x,now_y);
        break;
        case 6:
          draw_triangle(now_x,now_y);
        break;
        default:
      }
    }
  })
  canvas.addEventListener('mouseup',function(){
    push = false;
    prepare_push();
  })
})
/*
顏色選擇器  colorPicker
*/
var colorBlock = document.getElementById('color-block');
var ctx1 = colorBlock.getContext('2d');
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var colorLabel = document.getElementById('color-label');

var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(255,0,0,1)';

ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillGradient();
}

function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}

function mousedown(e) {
  drag = true;
  changeColor(e);
}

function mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}

function mouseup(e) {
  drag = false;
}

function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = rgbaColor;
  console.log('change!');
  ctx.strokeStyle = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2]+')';
  ctx.fillStyle = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2]+')';
}

colorStrip.addEventListener("click", click, false);

colorBlock.addEventListener("mousedown", mousedown, false);
colorBlock.addEventListener("mouseup", mouseup, false);
colorBlock.addEventListener("mousemove", mousemove, false);


